﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestDriven
{
    class Program
    {
        static void Main(string[] args)
        {
            char SPLIT = ' ';
            string[] entryHolder = new string[10];
            string[] names = new string[10];
            int[] priority = new int[10];
            string item;
            bool again = true;
            int i = 0;

            Console.WriteLine("Please enter a name followed by a number to indicate priority.\nOnce finished press enter.");
            do
            {
                item = Console.ReadLine();
                if (item == "")
                {
                    break;
                }
                entryHolder = item.Split(SPLIT);
                names[i] = entryHolder[0];
                priority[i] = int.Parse(entryHolder[1]);
                i++;
            } while (again);

            i = 0;

                for(int j = 0; j < priority.Length; j++)
                {
                    for (int k = 0; k < priority.Length -1; k++)
                    {
                            if (priority[k] < priority[k + 1])
                            {
                                int temp = priority[k + 1];
                                priority[k + 1] = priority[k];
                                priority[k] = temp;
                                string temporary = names[k + 1];
                                names[k + 1] = names[k];
                                names[k] = temporary;
                            }
                    }
                }
            for (int idea = 0; idea < names.Length; idea++)
            {
                Console.WriteLine(names[idea]);
            }

            Console.ReadLine();
        }
    }
}
